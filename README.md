## Dataset

The dataset can be found here: https://drive.google.com/file/d/1od3d58H86gFfdWPKAHL0pgHaBRkG-ppm/view?usp=sharing

## Overview

This is an autonomous racing dataset collected by UGRacing in July 2023 at the annual Formula Student competition. The dataset was collected during a fully autonomous 'autocross' event in which the vehicle completes a lap on a previously unseen track.

## Coordinate System

All data is in the standard ROS coordinate system. The X-axis points forwards, the Y-axis is to the left and the Z-axis is upwards. LiDAR point clouds and LiDAR cone observations are relative to the LiDAR sensor whereas camera images and observations are relative to the Camera. Odometry estimates are relative to the LiDAR sensor's starting position and describe the motion of the LiDAR sensor during the event.

## Sensor Data

The 'images' directory contains rectified camera images from a Stereolabs ZED2 camera mounted on the autonomous vehicle. The 'left' and 'right' subdirectories contain images from the left and right cameras respectively. Each filename contains the timestamp (in nanoseconds) at which the image was recorded.

The 'lidar' directory contains point clouds recorded from a VLP-16 LiDAR camera mounted on the autonomous vehicle. The 'xyz' subdirectory contains PCD point cloud files with only x,y,z positions for each point. The 'xyzir' subdirectory contains the same point clouds but with intensity readings and ring numbers for each point. Similarly to the camera data, the filename contains the timestamp (in nanoseconds) at which the point cloud was recorded.

There is a small transformation between the LiDAR and Camera sensors. The LiDAR is approximately 5cm infront and 10cm below the camera. The camera is pitched downwards (towards the ground) by less than 30 degrees. The LiDAR sensor is mounted flat on the front on the vehicle with its forward axis parallel to the vehicle.


## Observations

The 'lidar\_cones.csv' and 'camera\_cones.csv' files contain cone measurements from our LiDAR and camera perception systems respectively. Each entry is of the form 'time,x,y,z,colour', where the first is an integer timestamp in nanoseconds, the next three elements are floating point numbers and the final is an integer between 0-4 inclusive. These integers map to colours as follows:

* 0 = Blue
* 1 = Orange
* 2 = Large Orange
* 3 = Yellow
* 4 = Unknown

Note that no orange cones were observed during this event.

## Odometry

Odometry was esimtated using KissICP to match subsequent LiDAR scans. Odometry data is located within the 'odometry.csv' file. Each line is of the form 'time,x,y,z,qx,qy,qz,qw' where all values are floating point numbers except from the first, which is an integer timestamp in nanoseconds. The floating point values represent a pose by a translation and rotation represented by a quaternion. Poses are global and relative to the LiDAR sensor's starting position.

## Track Ground Truth

The track's ground truth cone positions were estimated using cone observations, point cloud readings and estimated odometry. These were hand labelled after some data processing. Each line in the 'labelled\_cones.csv' file is of the form 'x,y,z,colour', where the first three values are floating point values of a position vector and the final value is an integer colour code whose mapping is the same one used for observations.

## Contact

For any additional questions please contact eng-ugracing@glasgow.ac.uk
